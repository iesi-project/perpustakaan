/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package perpustakaan.controller.helper;

import java.util.ArrayList;
import perpustakaan.model.BukuDipinjam;
import perpustakaan.model.Peminjaman;
import perpustakaan.Perpustakaan;

/**
 *
 * @author tuary
 */
public class PeminjamanManager {
   
    public boolean save(ArrayList<BukuDipinjam> bukuDipinjamCollection){
        Perpustakaan.peminjaman = new Peminjaman(bukuDipinjamCollection);
        return true;
    }
}
